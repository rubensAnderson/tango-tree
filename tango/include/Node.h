#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    Node *left;
    Node *right;
    Node *parent;
    double key;
    bool isRoot;
    bool isLeaf;
    int depth; // this is a node in reference tree
    Node()
    {
        this->left = NULL;
        this->right = NULL;
        this->depth = -1;
        this->parent = NULL;
        this->key = -1;
        this->isRoot = false;
        this->isLeaf = false;
    }
    virtual ~Node(){
        delete this->left;
        delete this->right;
        delete this->parent;
    }
    void print();

protected:
private:
};

#endif // NODE_H
