#ifndef TANGO_H
#define TANGO_H

#include "BST.h"
#include "RBT.h"

#include <vector>
#include <iostream>

using namespace std;

class Tango
{
    public:
        Tango();
        virtual ~Tango();
/** 
 * 
*/
        RBTNode * root_aux;
        Node * root_reference;
        void search(RBTNode * n, double key);
        vector <RBTNode*> split(RBT * t, double key);
        RBT * concatenate(RBT & a, RBT & b);
        vector <RBTNode*> cut(RBT * r, double key);

    protected:

    private:
};

#endif // TANGO_H
