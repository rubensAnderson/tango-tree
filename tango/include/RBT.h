#ifndef RBT_H
#define RBT_H

#include "RBTNode.h"
#include "Node.h"

#include <math.h>
#include <iostream>
#include <vector>

using namespace std;

const string BLACK = "black"; 
const string RED = "red"; 

class RBT
{
public:
    RBTNode *root;

    RBT();
    virtual ~RBT();
    void leftRotate(RBTNode *x);
    void rightRotate(RBTNode *x);
    vector <RBT*> split(double x);

    void insert(Node *n);
    void insertionFixUp(RBTNode *z);

    void upToRoot(RBTNode *x);
    RBTNode *search(double x);

    void print();

    static int getBlackHeight(RBT *T);

    void concatenate(RBT * N);

    static RBT *concatenate(RBT & M, RBT & N);
    static vector<RBT*> split(RBT * R, double x);
    

protected:
private:
};

#endif // RBT_H
