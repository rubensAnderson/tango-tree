#ifndef RBTNODE_H
#define RBTNODE_H

#include <iostream>
#include <vector>
#include <math.h>
#include <Node.h>
#include<string>

using namespace std;

class RBTNode: public Node
{
public:
    RBTNode()
    {
        // super is called autom.
        this->maxDepth = -1;
        this->color = "red";
        this->blackHeight = -1;
        this->left = NULL;
        this->right = NULL;
        this->parent = NULL;
    }
    virtual ~RBTNode();
    int blackHeight;
    int maxDepth;
    RBTNode * left;
    RBTNode * right;
    RBTNode * parent;
    string color;
    
    void print();

protected:
private:
};

#endif // RBTNODE_H
