#ifndef BST_H
#define BST_H

#include <iostream>
#include <vector>

#include "Node.h"


class BST
{
    public:
        Node * root;
        BST();
        virtual ~BST();

    protected:
        Node * buildPerfectTree(vector <double> list, Node * parent);

    private:
};

#endif // BST_H
