#include "RBTNode.h"

#include <iostream>

using namespace std;


RBTNode::~RBTNode()
{
    //dtor
}

void RBTNode::print(){
    cout << "\n{\n";
    cout << "\n\tkey: " << this->key;
    cout << "\n\tcolor: " << this->color;
    //cout << "\n\tdepth: " << this->depth;
    //cout << "\n\tblack height: " << this->blackHeight;
    cout << "\n\tleaf? " << this->isLeaf;
    cout << "\n\troot? " << this->isRoot;
    cout << "\n\thas left? " << (this->left != NULL? this->left->key: -99);
    cout << "\n\thas right? " << (this->right != NULL? this->right->key: -99);
    cout << "\n\thas parent? " << (this->parent != NULL? this->parent->key: -99);
    cout << "\n}\n";
}
