#include "Node.h"


void Node::print(){
    cout << "\n{\n";
    cout << "\n\tkey: " << this->key;

    //cout << "\n\tdepth: " << this->depth;

    cout << "\n\tleaf? " << this->isLeaf; 
    cout << "\n\troot? " << this->isRoot;
    cout << "\n\thas left? " << (this->left != NULL? this->left->key: -1);
    cout << "\n\thas right? " << (this->right != NULL? this->right->key: -1);
    cout << "\n\thas parent? " << (this->parent != NULL? this->parent->key: -1);
    cout << "\n}\n";
}
