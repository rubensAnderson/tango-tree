#include "RBT.h"
#include <Node.h>
#include <RBTNode.h>

const string line = "\n_______________________\n";
RBT::RBT()
{
    this->root = NULL;
}

RBT::~RBT()
{
    delete this->root;
}

/**
 * Every call with node * is by value. But node only is by reference
 */

/**
 * This need a node from the reference tree.
 * Does consider fake leaf nodes and blackHeight(looks good)
 * @param {*} n
*/
/*
*/

void RBT::insert(Node *n)
{
    cout << "\n=======comecando a insercao (chave:" << n->key << ")======\n";
    RBTNode *node = new RBTNode();
    node->key = n->key;
    node->depth = n->depth;
    node->isLeaf = true;

    RBTNode *p = this->root;
    RBTNode *y = NULL;

    cout << " (insert) buscando folha...\n";

    bool found = false;
    while (!found)
    {
        if(p == NULL || p->isLeaf){
            found = true;
        }else{
            if (node->key < p->key){
                if(p->left == NULL){
                    found = true;
                }else{
                    p = p->left;
                }
            }
            else{
                if(p->right == NULL){
                    found = true;
                }else{
                    p = p->right;
                }
            }
        }
    }

    //found the leaf (or lonely this->root)
    y = p;

    node->parent = y;

    if (y == NULL)
    {
        cout << "\t (insert) !!!folha nula!!!\n";
        this->root = node;
        this->root->isLeaf = true;
        this->root->isRoot = true;
        this->root->blackHeight = 1; // this->root is black
        this->root->color = BLACK;
        cout << "nova raiz:" << this->root->key << endl;
        //this->root->print();
        return;
    }
    else
    {
        // cout << "folha encontrada:\n ";
        y->print();
        y->isLeaf = false;
        node->blackHeight = y->blackHeight; // node is red
        if (node->key < y->key)
        {
            y->left = node;
        }
        else
        {
            y->right = node;
        }
    }

    node->left = NULL;
    node->right = NULL;
    node->color = RED;
    node->isLeaf = true;
    insertionFixUp(node); // by value, because node must point to the same local, but in there must change
    cout << "\n=======terminando a insercao======\n";
}

/**
 * Mirror of right Rotate
*/
void RBT::leftRotate(RBTNode *x)
{
    cout << "\n == starting left rotating==" << x->key << endl;
    RBTNode *y = x->right;

    if (y == NULL)
    {
        return;
    }

    x->right = y->left;
    if (y->left != NULL)
        y->left->parent = x;
    y->parent = x->parent;
    if (x->parent == NULL)
    {
        this->root = y;
        this->root->isRoot = true;
        x->isRoot = false;
    }
    else if (x == x->parent->left)
    {
        x->parent->left = y;
    }
    else
    {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;

    if (y->isLeaf)
    {
        x->isLeaf = true;
        y->isLeaf = false;
    }

    if (x->left == NULL && x->right == NULL)
    {
        x->isLeaf = true;
    }

    cout << "==end left rotate==\n";
}

/**
 * Mirror of left Rotate
*/
void RBT::rightRotate(RBTNode *x)
{
    cout << "\n starting right rotating " << x->key << endl;
    RBTNode *y = x->left;

    if (y == NULL)
    {
        return;
    }

    x->left = y->right;
    if (y->right != NULL)
        y->right->parent = x;
    y->parent = x->parent;
    if (x->parent == NULL)
    {
        this->root = y;
        this->root->isRoot = true;
        x->isRoot = false;
    }
    else if (x == x->parent->right)
    {
        x->parent->right = y;
    }
    else
    {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;

    if (y->isLeaf)
    {
        x->isLeaf = true;
        y->isLeaf = false;
    }

    if (x->left == NULL && x->right == NULL)
    {
        x->isLeaf = true;
    }

    cout << "end right rotate\n";
}

void RBT::insertionFixUp(RBTNode *z)
{
    cout << "===comecando fixup===\n";
    if (z == NULL)
    {
        return;
        // cout << "\nz is null (insert fixup)\n";
    }

    while (z->parent != NULL && z->parent->color == RED)
    {
        // cout << "\n\t<> no da vez: " << z->key << endl;

        if (z->parent->parent == NULL)
        {
             cout << "\n (fixup) returning... grandpa is null\n";
            return;
        }

        RBTNode *grandpa = z->parent->parent;

        if (grandpa->right == z->parent)
        { // estamos na direita

            if (grandpa->left != NULL)
            {
                if (grandpa->left->color == RED)
                {
                    grandpa->left->color = BLACK;
                    grandpa->color = RED;
                    grandpa->blackHeight = grandpa->blackHeight - 1;
                    z->parent->color = BLACK;

                    z->blackHeight = z->parent->blackHeight;
                    z = z->parent->parent;
                }
                else
                {
                    // the hard case

                    if (z == z->parent->right)
                    {
                        z = z->parent;       // Case 2
                        this->leftRotate(z); // Case 2 by ref. rotates must update black height
                    }
                    z->parent->color = BLACK;
                    this->leftRotate(z->parent->parent); // changes from the left tree
                    this->leftRotate(z->parent);
                    this->rightRotate(z->parent);
                }
            }
            else
            {
                if(z == z->parent->left){
                    z = z->parent;
                    this->rightRotate(z);
                }
                z->parent->color = BLACK;
                z->parent->parent->color = RED;
                this->leftRotate(z->parent->parent);
                // cout << "\nfixed!\n";
                return;
            }
        }
        else
        { // estamos no filho da esquerrda
            if (grandpa->right != NULL)
            {
                if (grandpa->right->color == RED)
                {
                    grandpa->right->color = BLACK;
                    grandpa->color = RED;
                    grandpa->blackHeight = grandpa->blackHeight - 1;
                    z->parent->color = BLACK;

                    z->blackHeight = z->parent->blackHeight;
                    z = z->parent->parent;
                }
                else
                {
                    // the hard case

                    if (z == z->parent->left)
                    {
                        z = z->parent;       // Case 2
                        this->rightRotate(z); // Case 2 by ref. rotates must update black height
                    }
                    z->parent->color = BLACK;
                    this->rightRotate(z->parent->parent); // changes from the right tree
                    this->rightRotate(z->parent);
                    this->leftRotate(z->parent);
                }
            }
            else
            {
                if(z == z->parent->right){
                    z = z->parent;
                    this->leftRotate(z);
                }
                z->parent->color = BLACK;
                z->parent->parent->color = RED;
                this->rightRotate(z->parent->parent);
                // cout << "\nfixed!\n";
                return;
            }
        }
    }

    this->root->color = BLACK;
}

void printRec(RBTNode *n)
{
    if (n != NULL)
    {
        n->print();
        printRec(n->left);
        printRec(n->right);
    }
}

void RBT::upToRoot(RBTNode *x)
{
    cout << "==starting up to root==\n";
    if (x == NULL || x->parent == NULL)
    {
        return;
    }
    if (this->root == NULL)
    {
        // tem algo errado
        this->root = x;
    }
    while (x->parent != NULL && !x->isRoot)
    {
        if (x == x->parent->left)
        {
            this->rightRotate(x->parent);
        }
        else
        {
            this->leftRotate(x->parent);
        }
    }
    x->isRoot = true;
    cout << "==ending up to root==\n";
}

int RBT::getBlackHeight(RBT *T)
{
    cout << "==comecando bh\n";
    if(T == NULL){
        cout << "\t!!arvore nula (getBlackHeight)!!";
        return 0;
    }
    RBTNode *r = T->root;
    int bh = 0;
    while (r != NULL && !r->isLeaf)
    {
        if (r->color == BLACK)
        {
            bh++;
        }
        r = r->left;
    }
    cout << "==terminando bh\n";
    return bh;
}

RBTNode *RBT::search(double x)
{
    cout << "== buscando por " << x << endl;
    RBTNode *n = this->root;
    while (n != NULL && !n->isLeaf)
    {
        if (x == n->key)
        {
            return n;
        }
        if (x < n->key)
        {
            n = n->left;
        }
        else
        {
            n = n->right;
        }
    }
    cout << "== terminando busca por " << x << endl;
    return n;
}












vector<RBT*> RBT::split(RBT * R, double x){

    cout << "\n==comecando split==\n";
    
    vector<RBT *> re;

    if(R == NULL || R->root == NULL){
        cout << "\n\t (split) R is null\n";
        return re;
    }

    RBT *T2 = new RBT();
    RBT *T1 = new RBT();
    RBTNode *k = R->root;

    while (k != NULL)
    {
        
        cout << line << "\t (split) == k: " << k->key << endl;
        cout << "\t (split) == x: " << x << endl;
        cout << "\t (split) == T1: " << endl;
        T1->print();
        cout << "\t (split) == T2: " << endl;
        T2->print();
        cout << line;
        
        if (x < k->key)
        {
            cout << " (split) x eh menor. seguindo a esquerda\n";
            RBT *r = new RBT();
            
            // seguimos à esquerda
            r->root = k->right;
            
            if(r->root != NULL){
                r->root->isRoot = true;
                r->root->parent = NULL;
            }


            T2 = RBT::concatenate(*r, *T2);

            RBTNode *k2 = k;
            T2->insert(k2);

            k = k->left;
        }
        // the good case
        else if(x == k->key){
            cout << " (split) x igual. acabando...\n";

            RBT * R = new RBT();
            RBT * L = new RBT();

            R->root = k->right;
            if(R->root != NULL){
                R->root->isRoot = true;
                R->root->parent = NULL;
            }

            L->root = k->left;
            if(L->root != NULL){
                L->root->isRoot = true;
                L->root->parent = NULL;
            }

            cout << " (split) concatenando R e T2\n";
            T2 = RBT::concatenate(*R, *T2);
            cout << " (split) concatenando T1 e L\n";
            T1 = RBT::concatenate(*T1, *L);

            cout << " (split) inserindo " << k->key << endl;
            T1->insert(k);
            k = NULL;

        }
        else
        {
            cout << " (split) x eh maior. seguindo para direita\n";
            RBT * l = new RBT();
            l->root = k->left;
            if(l->root != NULL){
                l->root->isRoot = true;
                l->root->parent = NULL;
            }

            cout << " (split) concatenando l e T1\n";
            T1 = RBT::concatenate(*l, *T1);
            cout << " (split) inserindo " << k->key << endl;
            T1->insert(k);

            k = k->right;
        }
    }

    cout << "\n\t end of while\n";
    

    if(T1->root != NULL){
        T1->root->isRoot = true;
        T1->root->color = BLACK;
    }else{
        cout << " (split) T1 is null\n";
    }

    if(T2->root != NULL){
        T2->root->color = BLACK;
        T2->root->isRoot = true;
    }else{
        cout << " (split) T2 is null\n";
    }

    cout << "\n  (split) === Ao final, as arvores divididas:\n";

    cout << "\nT1: \n";
    T1->print();
    
    cout << "\nT2: \n";
    T2->print();
    

    re.push_back(T1);
    re.push_back(T2);

    cout << "\n==terminando split==\n";
    return re;

}











RBT *RBT::concatenate(RBT & M, RBT & N)
{
    cout << line << "...comecando a concatenar...";
    if(M.root == NULL){
        cout << "\n (concat) M is null\n";
        return &N;
    }

    if(N.root == NULL){
        cout << "\n (concat) N is null\n";
        return &M;
    }

    RBTNode *n1 = M.root;
    RBTNode *n2 = N.root;

    if (M.root->key >= N.root->key)
    {
        cout << "\tM maior... trocando (concat)\n";
        RBT aux = N;
        N = M;
        M = aux;
    }

    
    cout << "\n (concat) pegando a black height\n";
    int bh1 = RBT::getBlackHeight(&M);
    int bh2 = RBT::getBlackHeight(&N);

    cout << "\n (concat) black height M:" << bh1;
    cout << "\n (concat) black height N:" << bh2;

    RBTNode *n = N.root;
    RBTNode *m = M.root;

    cout << "\n (concat) root M:" << m->key;
    cout << "\n (concat) root N:" << n->key;

    if (bh1 > bh2)
    {
        cout << "\n\t (concat) bh1 > bh2\n";
        m = M.root;
        n = N.root;
        // desce em M com bh1 no caminho a direita

        while (m != NULL && !m->isLeaf && bh1 > bh2)
        {
            if (m->color == BLACK)
            {
                bh2++;
            }
            m = m->right; //*
        }

        // cout << " saiu com m:" << endl;
        // m->print();

        RBTNode *pai = new RBTNode();
        pai->key = (n->key + m->key) / 2;
        cout << "\n:.";
        pai->color = RED;

        pai->parent = m->parent;
        cout << ".";
        if (pai->parent == NULL)
        {
            pai->isRoot = true;
            M.root->parent = pai;
            M.root = pai;
        }
        else
        {
            m->parent->right = pai;
        }
        cout << ".";

        cout << ".:";
        pai->left = m;
        pai->right = n;
        

        N.root->parent = pai;
        N.root->isRoot = false;

        M.insertionFixUp(pai);

        cout << line << "...Terminando a concat...";
        return &M;
    }
    else
    {
        cout << "\n\t (concat) bh1 <= bh2\n";
        m = M.root;
        n = N.root;
        // desce em M com bh1 no caminho a direita

        while (n != NULL && !n->isLeaf && bh1 < bh2)
        {
            if (n->color == BLACK)
            {
                bh1++;
            }
            n = n->left; //*
        }

       

        RBTNode *pai = new RBTNode();
        cout << "\n:.";
        pai->key = (n->key + m->key) / 2;
        pai->color = RED;

        cout << ".";
        pai->parent = n->parent;
        cout << ".";
        if (pai->parent == NULL)
        {
            pai->isRoot = true;
            N.root->parent = pai;
            N.root->isRoot = false;
            N.root = pai;
        }
        else
        {
            n->parent->left = pai;
        }

        cout << ".";
        pai->right = n;
        pai->left = m;

        // cout << ".:\n pai:\n";
        // pai->print();
        

        M.root->parent = pai;
        M.root->isRoot = false;

        N.insertionFixUp(pai);
        

        cout << line << "...Terminando a concat...";
        return &N;
    }
}

/**
*/
void RBT::print()
{
    cout << "PRINTANDO A ARVORE RN\n";
    printRec(this->root);
}
