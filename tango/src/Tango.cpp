#include "Tango.h"

#include "Tango.h"
#include "Node.h"
#include "RBTNode.h"
#include <math.h>
#include <string>

using namespace std;

Tango::Tango()
{
    //ctor
}

Tango::~Tango()
{
    //dtor
}



//void findMaxDepthNode(A) {}


//void findMinDepthNode(A) {}

void markRoot(RBT A) {
    A.root->isRoot = true;
    A.root->color = "black";
}


RBTNode * searchBin(RBTNode * A, double key) {
    if (A == NULL) {
        return NULL;
    }
    if (A->isLeaf) {
        if (A->key == key) {
            return A;
        }
        return NULL;
    }
    if (key > A->key) {
        return searchBin(A->right, key);
    }
    if (key < A->key) {
        return searchBin(A->left, key);
    }
    return A;
}


void Tango::search(RBTNode * R, double key){
    RBTNode * n = searchBin(R, key); // R is the root of a auxiliary tree
    if(R == NULL){
        cout << "Chave " << key << " nao encontrada!\n";
        return;
    }
    if(n->key == key){
        //return n;
        cout << "FOUND! Node:";
        n->print();
    }

    // dunno yet!
    if(n->key < key){
        // this.cut();
        // this.join();
        // this.search(n->left, key);
    }else{
        // this.cut();
        // this.join();
        // this.search(R->right, key);
    }
}








vector <RBTNode*> Tango::split(RBT*t, double x){
    //  * 1. Let T1 ←− ∅, and T2 ←− ∅.
    //why i cant do this?
    RBT T1;
    RBT T2;

    //  * 2. Let κ ←− root(T ).
    RBTNode *k = t->root;
    //  * 3. While κ is not a leaf node do:
    while (!k->isLeaf)
    {
        // • If x < key(κ), then:
        if (x < k->key)
        {
            RBT rig;
            rig.root = k->right;
            //  *      Catenate the subtree rooted at right(κ) to T2.
            this->concatenate(rig, T2);
            //  *      Let κ ←− left(κ).
            k = k->left;

            // • Otherwise (if x ≥ key(κ)):
        }
        else
        {
            RBT rig;
            rig.root = k->left;
            //  *      Catenate the subtree rooted at left(κ) to T1.
            this->concatenate(rig, T2);
            //  *      Let κ ←− right(κ)
            k = k->right;
        }
    }

    //  * 4. Insert the element stored at κ into T1 if it is less than x,
    if (k->key < x)
    {
        //T1.insert(k); // make them mother and son
    }
    //  * otherwise insert it into T2.
    else
    {
        //T2.insert(k);
    }

    vector <RBTNode *> re;
    re.push_back(T1.root);
    re.push_back(T2.root);
    return re;
}


RBT * Tango::concatenate(RBT & T1, RBT & T2){
    // T2.blackHeight > T1.blackHeight

    RBTNode * t1 = T1.root;
    RBTNode * t2 = T2.root;

    //  * 1. Go down from root(T2) along the leftmost path in the tree, until reaching a black node ρ such
    //  * that black height(ρ) = black height(T1)
    RBTNode *p = t2;
    while (p->blackHeight > t1->blackHeight)
    {
        p = p->left;
    }
    //  * 2. Let π ←− parent(ρ).
    RBTNode *pi = p->parent;
    //  * 3. Make root(t1) and ρ siblings by creating a new node ν, and setting left(ν) ←− root(t1),
    //  * right(ν) ←− ρ and color(ν) ←− Red.
    t1->isRoot = false;
    RBTNode v;

    v.left = t1;
    v.right = p;
    v.color = RED;
    RBT V;
    //  * 4. If π = Null, set root(t1) ←− ν.
    if (pi == NULL)
    {
        v.isRoot = true;
        V.root = &v;
        t1 = &v;
    }
    //  * Othe;rwise, set parent(ν) ←− π and root(t1) ←− root(t2)->
    else
    {
        v.parent = pi;
        t1 = t2;
    }
    //  * 5. Perform the insertion fix-up procedure from the node ν.
    T1.insertionFixUp(&v);
    //  * 6. Mark t2 as empty by setting root(t2) ←− Null.
    t2 = NULL;

    return &T1;
}



RBTNode * build(Node * n){
    RBT * R;
    RBTNode * roott = NULL;

    while(n->right != NULL){

        RBT * rs = new RBT();
        while(n->left != NULL){
            rs->insert(n); // inserting a Node, not a RBTNode.
        }
        // first tree?
        if(roott == NULL){
            roott = rs->root;
            R = rs;
        }else{
            //RBTNode * max = maxRBTNode(R); // must be a leaf.
            // rs->root->parent = max;
            R = rs;
        }
        n = n->right;
    }

    return roott;
}




void maxRBTNode(RBTNode & maxNodeReferenc, RBTNode * root) {
    RBTNode * p = root;
    while (!p->isLeaf || p->right != NULL) {
        p = p->right;
    }
    maxNodeReferenc = *p;
}
/**

vector <RBTNode*> cut(RBT * A, double key);
    //getting the extreme nodes...
    var lp = this.findMinDepthNode(A)
    var rp = this.findMaxDepthNode(A)

    // here, lp turns into a root of a RBTree...
    RBT.split(A, lp);

    // now, rp turns into a root of a RBTree
    RBT.split(lp.right, rp);

    // mark root of D; this must update all the depths?
    this.markRoot(rp.left);

    // merges the two sons of rp into a new RBTree
    RBT.concatenate(rp.left, rp.right)

    // merges the two sons of lp into a new RBTree
    RBT.concatenate(lp.left, lp.right)

    // the result is A with append D
}

/**



magicalThings() {
    const solveAllMyProblemsBySayingItsEasy = NULL;
    return solveAllMyProblemsBySayingItsEasy;
}


join(A, B) {
    if (A.maxDepth > B.maxDepth) {
        // observe that the nodes in B have key values that fall in between two adjacent keys lp and rp in A
        // this.searchBin(A, B.key)
        lp = this.magicalThings();
        rp = this.magicalThings();

        this.split(A, lp);
        this.split(A, rp);

        this.unmarkRoot(B);

        this.concatenate(A, rp)
        this.concatenate(A, lp)
    }
}

 */

