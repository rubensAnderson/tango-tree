#include "BST.h"
#include <math.h>
#include <iostream>
#include <vector>

#include "json.hpp"
#include <math.h>

using json = nlohmann::json;

using namespace std;

std::ostream& operator<<(std::ostream& os, const std::vector<double> &input)
{
	for (auto const& i: input) {
		os << i << " ";
	}
	return os;
}




vector <double> createExponentialList(int a){
    vector <double> l;
    for(double i = 0; i < pow(2.0, a) - 1; i++){
        l.push_back(i);
    }

    cout << "lista criada: " << l;
    
    return l;
}






vector <double> partir(vector<double> list, int l, int r)
{
    vector <double> re;
    if(list.empty() || l > r || r < 0){
        return re;
    }

    if(l < 0){
        l = 0;
    }
    
    if(r > list.size()){
        r = list.size();
    }


    for(int i = l; i <= r; i++){
        re.push_back(list[i]);
    }

    return re;
    
}








BST::BST()
{
    //ctor
    this->root = this->buildPerfectTree(createExponentialList(2), nullptr);
}

BST::~BST()
{
    //dtor
}


Node * BST::buildPerfectTree(vector<double> list, Node *parent)
{
    cout << "\nstarting to build BST size " << list.size() << endl;

    if (list.size() == 0)
    {
        return parent;
    }
    
    Node * T = new Node();
    T->parent = parent;

    if(parent != NULL){
        T->isRoot = false;
        T->depth = parent->depth + 1;
    }else{
        T->isRoot = true;
        T->depth = 0;
    }

    int half = floor((list.size()) / 2) - (list.size() % 2==0 ? 1 : 0 );

    if(list.size() <= 1){
        T->key = list[0];
        T->print();
        return T;
    }
    vector <double> left = partir(list, 0, half);
    vector <double> right= partir(list, half+1, list.size()-1);

    T->key = left[left.size()-1];
    left.pop_back();

    cout << "left: " << left << endl;
    cout << "right: " << right << endl;


    T->left = this->buildPerfectTree(left, T);
    T->right = this->buildPerfectTree(right, T);

    T->print();

    return T;
}


