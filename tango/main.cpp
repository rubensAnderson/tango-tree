#include <vector>
#include <ctime>
#include <iostream>

#include <Node.h>
#include <RBTNode.h>
#include <BST.h>
#include <RBT.h>

using namespace std;


vector<Node*> generateNodes(int n){
    vector <Node*> nodes;
    for(int i = 0; i < n; i++){
        Node * node = new Node();

        node->key = (double)i+1;
        node->depth = 0;

        nodes.push_back(node);
    }

    return nodes;
}

vector<Node*> generateNodes(int a, int b){
    vector <Node*> nodes;
    for(int i = a; i < b; i++){
        Node * node = new Node();

        node->key = (double)i+1;
        node->depth = 0;

        nodes.push_back(node);
    }

    return nodes;
}

RBT * generateRBT(int a, int b){
    RBT * r = new RBT();

    vector <Node*> nodes = generateNodes(a, b);
    for(int i = 0; i < nodes.size(); i++){
        r->insert(nodes[i]);
    }

    return r;
}

RBT * generateRBT(int n){
    RBT * r = new RBT();

    vector <Node*> nodes = generateNodes(n);
    for(int i = 0; i < nodes.size(); i++){
        r->insert(nodes[i]);
    }

    return r;
}

void testBST(){
    BST * p = new BST();
}

void testRBT(int kk){
    vector<Node*> n = generateNodes(5, 50);

    RBT * R = new RBT();

    int max = kk;

    max = max > n.size()? n.size():max;

    for(int i = 0; i < max; i++){
        cout << "node " << n[i]->key << ": \n";

        R->insert(n[i]);
    }

    R->print();
    cout << "\n\n...Terminando...";
}

vector <Node*> generateRandomNodes(int kk){
    srand((unsigned)time(0));

    vector <Node*> nodes;
    for(int i = 0; i < kk; i++){
        Node * node = new Node();

        node->key = 1 + (rand()%100);
        node->depth = 0;

        nodes.push_back(node);
    }

    return nodes;
}



vector <Node*> generateRandomNodes(int kk, int minn, int maxx){
    srand((unsigned)time(0));

    vector <Node*> nodes;
    for(int i = 0; i < kk; i++){
        Node * node = new Node();

        node->key = 1 + (rand()%maxx) + minn;
        node->depth = 0;

        nodes.push_back(node);
    }

    return nodes;
}


RBT * generateRandomRBT(int kk){
    vector<Node*> n = generateRandomNodes(kk);
    for(int i = 0; i < n.size(); i++){
        cout << n[i]->key << endl;
    }

    RBT * R = new RBT();

    for(int i = 0; i < n.size(); i++){
        cout << n[i]->key << endl;
        R->insert(n[i]);
    }

    return R;
}



RBT * generateRandomRBT(int kk, int minn, int maxx){
    vector<Node*> n = generateRandomNodes(kk, minn, maxx);
    for(int i = 0; i < n.size(); i++){
        cout << n[i]->key << endl;
    }

    RBT * R = new RBT();

    for(int i = 0; i < n.size(); i++){
        cout << n[i]->key << endl;
        R->insert(n[i]);
    }

    return R;
}




void testRandomRBT(int kk){

    RBT * R = generateRandomRBT(kk);

    R->print();
}









void testSplit(){
    RBT * T = generateRBT(10);
    T->print();
    cout << "\n\n comecando split\n\n";
    vector<RBT *> S = RBT::split(T, 5.0);
}

void testConcatenate(){
    cout << "\n\n ==comecando concatenate==\n\n";
    RBT * T1 = generateRandomRBT(3, 0, 5);
    RBT * T2 = generateRandomRBT(10, 6, 15);

    cout << "T1:\n";
    T1->print();
    cout << "\nT2:\n";
    T2->print();

    RBT * S = RBT::concatenate(*T1, *T2);
    cout << "\nS:\n";
    S->print();
    cout << "\n\n ==terminando concatenate==\n\n";
}

int main()
{
    cout << "...Comecando...\n\n";

    testSplit();

    cout << "Terminando\n\n";

    return 0;
}
